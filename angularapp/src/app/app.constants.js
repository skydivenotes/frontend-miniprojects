(function () {
    'use strict';

    angular
    .module('tNotes')
    .constant('API_URL', {
        GET_FORM: "http://api.dev.skydivenotes.com/v1.0/form/{{form_id}}",

        LOG: "http://api.dev.skydivenotes.com/v1.0/log",
        LOG_ID: "http://api.dev.skydivenotes.com/v1.0/log/{{id}}",

        //GET_FORM: "assets/form.json",
        GET_CHIPS_USERS: "http://api.dev.skydivenotes.com/v1.0/user/filter/{{query}}",
        GET_CHIPS_PRODUCTCODE: "http://api.dev.skydivenotes.com/v1.0/product/code/{{query}}",
        GET_CURRENT_USER: "http://api.dev.skydivenotes.com/v1.0/user/me",
        GET_MENU: "http://api.dev.skydivenotes.com/v1.0/menu",
        GET_LOG_CHART: "http://api.dev.skydivenotes.com/v1.0/log/chart",
        GET_LOG_PIECHART: "assets/logPieChart.json",
        GET_LOG_DATA: "http://api.dev.skydivenotes.com/v1.0/log",
        GET_COMPANIES: "assets/companies.json",
        GET_NAVBAR: "assets/navbar.json",
        GET_NEWS: "assets/news.json",
        GET_WEBSOCKET: "ws://ws.dev.skydivenotes.com"
    });
})();
