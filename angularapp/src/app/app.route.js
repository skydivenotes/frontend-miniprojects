(function () {
    'use strict';

    angular
    .module('tNotes')
    .config(routerConfig);

    function routerConfig($stateProvider, $urlRouterProvider) {
        $stateProvider
        .state('app', {
            url: "/app/home",
            templateUrl: "app/main/main.html",
            controller: 'MainController',
            controllerAs: 'vm'
        });

        $urlRouterProvider.otherwise('/app/home');
    }

})();
