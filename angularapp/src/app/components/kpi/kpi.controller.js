(function () {
    'use strict';

    angular
    .module('tNotes')
    .controller('KpiController', KpiController);

    function KpiController($rootScope, LogService, sharedProperties) {
        var vm = this;
        vm.query = "";
        vm.items = [];
        vm.debug_url = "";

        $rootScope.pageTitle = "KPI";

        vm.filters = sharedProperties.getValue("filters.kpi");

        vm.debug_url = LogService.debugParams("kpi", vm.filters);
        $rootScope.$on("filters.kpi", function ($event, filters) {
            vm.debug_url = LogService.debugParams("kpi", filters);
        });

    }

})();
