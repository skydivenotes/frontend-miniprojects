(function () {
    'use strict';

    angular
    .module('tNotes')
    .config(routerConfig);

    function routerConfig($stateProvider) {
        $stateProvider
        .state('kpi', {
            url: "/app/kpi",
            templateUrl: "app/components/kpi/kpi.html",
            controller: 'KpiController',
            controllerAs: 'vm'
        });
    }

})();
