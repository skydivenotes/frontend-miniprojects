(function () {
    'use strict';

    angular
    .module('tNotes')
    .directive('quickview', quickview);

    function quickview() {
        var directive = {
            restrict: "E",
            transclude: true,
            templateUrl: "app/components/quickview/quickview.html",
            controller: 'QuickViewController',
            controllerAs: 'vm'
        };

        return directive;
    }

})();
