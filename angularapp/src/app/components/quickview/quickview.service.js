(function () {
    'use strict';

    angular
    .module('tNotes')
    .service('QuickviewService', QuickviewService);

    function QuickviewService($http, API_URL, $q, sharedProperties, $rootScope) {
        var service = {};
        var menu = null;
        var previousFilters = null;

        angular.extend(service, {
            getMenu: getMenu,
            broadcastFilters: broadcastFilters
        });

        function getMenu() {
            if (menu)
                return menu;
            var deferred = $q.defer();
            $http.get(API_URL.GET_MENU).then(function (response) {
                deferred.resolve(response.data);
            });
            menu = deferred.promise;
            return menu;

        }

        function broadcastFilters(filters) {
            if (!Object.keys(filters).length)
                return;
            for (var filterName in filters) {
                var filter = filters[filterName];
                if (!angular.equals(previousFilters && previousFilters[filterName], filter)) {
                    var resFilters = {};
                    if (filter.form_id) {
                        resFilters.form_id = filter.form_id;
                    }
                    if (filter.daterange) {
                        resFilters.date_from = filter.daterange.startDate._d ? filter.daterange.startDate.format("YYYY-MM-DD") : filter.daterange.startDate;
                        resFilters.date_to = filter.daterange.endDate._d ? filter.daterange.endDate.format("YYYY-MM-DD") : filter.daterange.endDate;
                    }
                    resFilters.business_id = getFilterStr(filter.business_id);
                    resFilters.related_to_id = getFilterStr(filter.related_to_id);
                    resFilters.product_code = getFilterStr(filter.product_code);
                    resFilters.diviseller_id = getFilterStr(filter.diviseller_id);
                    resFilters.salesperson_id = getFilterStr(filter.salesperson_id);
                    resFilters.owner_id = getFilterStr(filter.owner_id);
                    resFilters.status = getFilterStr(filter.status);
                    resFilters.display_format = getFilterStr(filter.display_format);
                    resFilters.tags = getFilterStr(filter.tags);

                    $rootScope.$emit(filterName, resFilters);
                    sharedProperties.setValue(filterName, resFilters);
                }
            }
            previousFilters = angular.copy(filters);
        }

        function getFilterStr(filterObj) {
            var res;
            if (Object.prototype.toString.call(filterObj) === '[object Array]')
                res = filterObj;
            else if (typeof filterObj === 'string' || filterObj instanceof String)
                return filterObj;
            else
                res = idsObjectToArr(filterObj);
            if (res && res.length)
                return res.join(',');
            return null;
        }

        function idsObjectToArr(obj) {
            var arr = [];
            for (var prop in obj) {
                if (obj[prop]) {
                    arr.push(prop);
                }
            }
            return arr;
        }

        return service;
    }

})();
