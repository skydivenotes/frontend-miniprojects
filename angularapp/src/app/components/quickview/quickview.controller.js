(function () {
    'use strict';

    angular
    .module('tNotes')
    .controller('QuickViewController', QuickViewController);

    function QuickViewController($rootScope, sharedProperties, LogService, QuickviewService) {
        var vm = this;

        // get the sidebar menu
        QuickviewService.getMenu().then(function (menu) {
            vm.menu = menu;
            /*// emit a menu loaded event
            $rootScope.$emit("menuLoaded", menu);
            sharedProperties.setValue("menu", menu);*/
        });

        // define filters
        vm.filters = {};

        vm.form = { fields: null, model: null };

        // execute this when user clicks on the menu item
        vm.selectMenuItem = function (item) {
            if (item.form_id)
                vm.filters.filters.form_id = item.form_id;
            if (item.openTab)
                vm.openTab(item.openTab);
            $rootScope.$emit("menuItemSelect", item);
            sharedProperties.setValue("selectedMenuItem", item);
        };

        // share filters
        $rootScope.$watch("vm.filters", function (filters) {
            QuickviewService.broadcastFilters(filters);
        }, true);

        vm.loadForm = function (form_id) {
            vm.form.fields = null;
            vm.form.model = {};
            return LogService.getFormById(form_id).then(function (form) {
                vm.form.fields = form;
                vm.form.model.form_id = form_id;
            });
        };

        // post a form
        vm.form.onSubmit = function () {
            if (vm.form.model.id) {
                vm.form.onUpdate();
                return;
            }
            LogService.saveForm(vm.form.model).then(function(data){
                $rootScope.$emit("log:add", data);
                vm.hideTabs();
                vm.form.options.resetModel();
            });
        };

        vm.form.onUpdate = function() {
            LogService.updateById(vm.form.model.id, vm.form.model).then(function(data){
                $rootScope.$emit("log:update", data);
                vm.hideTabs();
                vm.form.options.resetModel();
            });
        };

        vm.cancel = function() {
            vm.hideTabs();
            vm.form.options.resetModel();
        };

        // call a form for edit
        $rootScope.$on("form:edit", function ($event, data) {
            vm.loadForm(data.form_id).then(function(form){
                vm.form.model = data;
            });
        });

        //-----------------------------
        // menu tabs logic
        vm.hide = false;
        vm.tabs = {};
        vm.tabInit = function (tabName) {
            vm.tabs[tabName] = false;
        };
        vm.openTab = function (name) {
            vm.hideTabs();
            vm.tabs[name] = true;
        };
        vm.hideTabs = function () {
            angular.forEach(vm.tabs, function (value, key) {
                vm.tabs[key] = false;
            });
            vm.form.fields = null;
        };
        vm.isOpenedTab = function () {
            var res = false;
            angular.forEach(vm.tabs, function (value, key) {
                if (value)
                    res = true;
            });
            return res;
        };
        $rootScope.$on('form:edit', function (data) {
            vm.openTab('forms')
        });


    }

})();
