(function () {
    'use strict';

    angular
    .module('tNotes')
    .controller('RadiogroupController', RadiogroupController);

    function RadiogroupController($scope) {
        var vm = this;
        vm.items = $scope.items;
        vm.placeholder = $scope.placeholder;
        $scope.$watch(function(){return vm.ngModel}, function(val){
           $scope.ngModel = val;
        }, true);
    }

})();
