(function () {
    'use strict';

    angular
    .module('tNotes')
    .directive('radioGroup', radioGroup);

    function radioGroup() {
        var directive = {
            restrict: "E",
            transclude: true,
            scope: {
                ngModel: '=',
                placeholder: '@',
                items: '='
            },
            templateUrl: "app/components/radiogroup/radiogroup.html",
            controller: 'RadiogroupController',
            controllerAs: 'vm'
        };

        return directive;
    }

})();
