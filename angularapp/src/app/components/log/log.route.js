(function () {
    'use strict';

    angular
    .module('tNotes')
    .config(routerConfig);

    function routerConfig($stateProvider) {
        $stateProvider
        .state('log', {
            url: "/app/log",
            templateUrl: "app/components/log/log.html",
            controller: 'LogController',
            controllerAs: 'vm'
        });
    }

})();
