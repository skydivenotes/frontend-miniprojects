(function () {
    'use strict';

    angular
    .module('tNotes')
    .controller('LogController', LogController);

    function LogController(LogService, $rootScope, sharedProperties, $state, $mdDialog, $timeout, API_URL, $httpParamSerializer) {

        var vm = this;
        vm.paging = {};
        vm.paging.limit = 5;
        vm.paging.page = 1;
        vm.debug_url_table = "";
        vm.debug_url_chart = "";

        if (!sharedProperties.getValue("selectedMenuItem")) {
            $state.go("app"); // if somebody isn't here from the menu click then go to the home page
            return;
        }

        vm.filters = sharedProperties.getValue("filters");

        $rootScope.$on("filters", function ($event, filters) {
            vm.filters = filters;
            refreshComponents();
        });

        function refreshComponents() {
            // get data for the log chart
            LogService.getLogChart(vm.filters).then(function (options) {
                AmCharts.makeChart("chartLog", options);
            });
            // get data for the table
            LogService.getTableData(vm.filters, vm.paging.page, vm.paging.limit).then(function (result) {

                vm.debug_url_table = LogService.debugParams(API_URL.GET_LOG_DATA, vm.filters, {
                    offset: vm.paging.page * vm.paging.limit - vm.paging.limit,
                    limit: vm.paging.limit
                });
                vm.debug_url_chart = LogService.debugParams(API_URL.GET_LOG_CHART, vm.filters);
                
                vm.tableColumns = result.columns;
                vm.tableRows = result.rows;
                vm.paging.total = result.total;
            });
        }

        $rootScope.$watch(function () {
            return vm.paging.page;
        }, function (page) {
            if (!page) return;
            refreshComponents();
        }, true);

        vm.edit = function (id) {
            LogService.getById(id).then(function (data) {
                $rootScope.$emit('form:edit', data);
            });
        };

        // do insert a row after the form submit
        $rootScope.$on("log:add", function ($event, data) {
            vm.tableRows.unshift(data);
        });

        // do update a row after the form submit
        $rootScope.$on("log:update", function ($event, data) {
            vm.tableRows.map(function (item) {
                if (item.id === data.id) {
                    angular.extend(item, data);
                    item.updateBlinking = true;
                    $timeout(function () {
                        item.updateBlinking = false;
                    }, 2000)
                }
            });
        });

        vm.delete = function (id) {
            var confirm = $mdDialog.confirm({
                title: 'Delete',
                content: 'Are you sure you want to delete this item?',
                ok: 'ok',
                cancel: 'cancel'
            });
            $mdDialog.show(confirm).then(function (answer) {
                LogService.deleteById(id).then(function () {
                    vm.tableRows = vm.tableRows.filter(function (item) {
                        return (item.id !== id);
                    });
                    confirm = undefined;
                });
            }).finally(function () {
                confirm = undefined;
            });
        };

    }

})();
