(function () {
    'use strict';

    angular
    .module('tNotes')
    .service('LogService', LogService);

    function LogService($http,
                        API_URL,
                        logPieOptions,
                        $q,
                        $interpolate,
                        $httpParamSerializerJQLike,
                        QuickviewService,
                        $httpParamSerializer
                        ) {

        var service = {};

        angular.extend(service, {
            getLogChart: getLogChart,
            getLogPieChart: getLogPieChart,
            getTableData: getTableData,
            getCompanies: getCompanies,
            getFormById: getFormById,
            getById: getById,
            saveForm: saveForm,
            updateById: updateById,
            debugParams: debugParams,
            deleteById: deleteById
        });

        function getTableData(filters, page, limit) {
            var params = {
                offset: page * limit - limit,
                limit: limit
            };
            return $http({
                url: API_URL.GET_LOG_DATA,
                method: 'GET',
                params: angular.extend(params, filters)
            })
            .then(function (response) {
                return {
                    columns: response.data.columns,
                    rows: response.data.rows,
                    total: response.data.total
                };
            });
        }

        function getFormById(form_id) {
            var url = $interpolate(API_URL.GET_FORM)({
                form_id: form_id
            });

            var deferred = $q.defer();

            $http.get(url, {}).then(function (response) {
                deferred.resolve(angular.fromJson(response.data));
            }).catch(function (error) {
                deferred.reject(angular.fromJson(error.data));
            });
            return deferred.promise;
        }

        // get data by id
        function getById(id) {
            var url = $interpolate(API_URL.LOG_ID)({
                id: id
            });
            var deferred = $q.defer();
            $http.get(url, {}).then(function (response) {
                deferred.resolve(angular.fromJson(response.data));
            }).catch(function (error) {
                deferred.reject(angular.fromJson(error.data));
            });
            return deferred.promise;
        }

        // save form
        function saveForm(formData) {
            var deferred = $q.defer();
            $http({
                url: API_URL.LOG,
                method: 'POST',
                data: $httpParamSerializerJQLike(formData),
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).then(function (response) {
                deferred.resolve(angular.fromJson(response.data));
            }).catch(function (error) {
                deferred.reject(angular.fromJson(error.data));
            });
            return deferred.promise;
        }

        function updateById(id, formData) {
            var url = $interpolate(API_URL.LOG_ID)({id: id});
            var deferred = $q.defer();
            $http({
                url: url,
                method: 'PUT',
                data: $httpParamSerializerJQLike(formData),
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).then(function (response) {
                deferred.resolve(angular.fromJson(response.data));
            }).catch(function (error) {
                deferred.reject(angular.fromJson(error.data));
            });
            return deferred.promise;
        }

        function deleteById(id) {
            var url = $interpolate(API_URL.LOG_ID)({id: id});
            var deferred = $q.defer();
            $http.delete(url).then(function (response) {
                deferred.resolve(response.data);
            }).catch(function (error) {
                deferred.reject(error.data);
            });
            return deferred.promise;
        }

        //----------------------------------
        // CHARTS

        function getLogPieChart() {
            return $http.get(API_URL.GET_LOG_PIECHART).then(function (response) {
                logPieOptions.dataProvider = response.data;
                return logPieOptions;
            });
        }

        function getLogChart(filters) {
            function generateAdditionalOptions(data, companies) {
                var graphOptions = {
                    "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
                    "fillAlphas": 1,
                    "labelText": "[[value]]",
                    "lineAlpha": 0,
                    "type": "column",
                    "color": "#000000"
                };
                var options = {
                    "type": "serial",
                    "theme": "light",
                    "dataProvider": data,
                    "categoryField": "date",
                    "graphs": [],
                    "categoryAxis": {
                        "gridPosition": "start",
                        "axisAlpha": 0,
                        "gridAlpha": 0,
                        "position": "left"
                    },
                    "export": {
                        "enabled": true
                    }
                };
                companies.map(function (company) {
                   // if (filters.business_id[company.id])
                        options.graphs.push(angular.extend({}, graphOptions, {
                            "title": company.name,
                            "valueField": company.key,
                            "fillColors": company.color
                        }));
                });
                return options;
            }

            function getChart() {
                return $http({
                    url: API_URL.GET_LOG_CHART,
                    method: 'GET',
                    params: filters
                });
            }

            return $q.all([
                getChart(),
                this.getCompanies()])
            .then(function (val) {
                return generateAdditionalOptions(val[0].data, val[1]);
            });
        }

        function getCompanies() {
            var deffered = $q.defer();
            QuickviewService.getMenu()
            .then(function (menu) {
                menu.filters.map(function (item) {
                    if (item.name === 'filters') {
                        item.controls.map(function (control) {
                            if (control.companies) {
                                deffered.resolve(control.companies);
                            }
                        })
                    }
                });
            })
            .catch(function (err) {
                deffered.resolve(err);
            });
            return deffered.promise;
        }

        function debugParams(url) {
            var resObj = {};
            for (var i = 1; i < arguments.length; i++) {
                angular.extend(resObj, arguments[i]);
            }
            var resUrl = url + "?" + $httpParamSerializer(resObj);
            return resUrl;
        }

        return service;
    }

})();
