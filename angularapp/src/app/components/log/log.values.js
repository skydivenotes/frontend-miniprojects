/* here is options for amcharts graphs */
(function () {
    "use strict";
    var pathToImages = "/assets/images/amcharts/";
    angular
    .module("tNotes")

    // amstock
    .value("logChartOptions", function () {
        return {
            "type": "stock",
            "theme": "light",
            "pathToImages": pathToImages,
            "addClasses": true,
            "dataSets": [
                {
                    "categoryField": "date"
                }
            ],
            "panels": [
                {
                    "title": "Volume",
                    "showCategoryAxis": true,
                    "autoMargins": true,
                    "chartCursor": {
                        "valueLineEnabled": true,
                        "valueLineBalloonEnabled": true,
                        "categoryBalloonAlpha": 0.8,
                        "valueLineAlpha": 0.5,
                        "fullWidth": true,
                        "cursorAlpha": 0.05
                    },
                    "valueAxes": [
                        {
                            "stackType": "regular",
                            "gridColor": "#dfdfdf",
                            "gridAlpha": "1",
                            "axisAlpha": 1
                        }
                    ]
                }
            ],
            "chartScrollbarSettings": {
                "enabled": false
            },
            "CategoryAxis": {
                "labelFrequency": 0.5
            },
            "categoryAxesSettings": {
                "minPeriod": "MM",
                "gridAlpha": 0,
                "axisAlpha": 0.5
            },
            "valueAxesSettings": {
                "axisAlpha": 0.5,
                "labelOffset": -45
            },
            "export": {
                "enabled": true
            }
        }
    }())

    //-----------------------
    // amcharts pie
    .value("logPieOptions", {
            "type": "pie",
            "theme": "light",
            "pullOutRadius": "8%",
            "innerRadius": "45%",
            "startDuration": 0,
            "color": "white",
            "outlineThickness": 0,
            "dataProvider": [],
            "valueField": "count",
            "addClassNames": true,
            "titleField": "person",
            "labelText": "[[title]]",
            "labelRadius": "-27%",
            "balloon": {
                "fixedPosition": true
            },
            "export": {
                "enabled": true
            }
        }
    )
})();
