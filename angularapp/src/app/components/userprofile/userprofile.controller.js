(function () {
    'use strict';

    angular
    .module('tNotes')
    .controller('UserprofileController', UserprofileController);

    function UserprofileController(UserprofileService, $scope, $translate, $rootScope) {
        var vm = this;

        // set page title
        $rootScope.pageTitle = "User profile";


        vm.languages = [{"key": "en", "value": "English"}, {"key": "fr", "value": "Français"}];
        vm.language = "en";

        $scope.$watch(function () {
            return vm.language
        }, function (lang) {
            $translate.use(lang);
        });
    }

})();
