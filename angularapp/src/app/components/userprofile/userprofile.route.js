(function () {
    'use strict';

    angular
    .module('tNotes')
    .config(routerConfig);

    function routerConfig($stateProvider) {
        $stateProvider
        .state('userprofile', {
            url: "/app/userprofile",
            templateUrl: "app/components/userprofile/userprofile.html",
            controller: 'UserprofileController',
            controllerAs: 'vm'
        });
    }

})();
