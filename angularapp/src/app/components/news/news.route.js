(function () {
    'use strict';

    angular
    .module('tNotes')
    .config(routerConfig);

    function routerConfig($stateProvider) {
        $stateProvider
        .state('news', {
            url: "/app/news",
            data : { pageTitle: 'News' },
            templateUrl: "app/components/news/news.html",
            controller: 'NewsController',
            controllerAs: 'vm'
        })
        .state('news-page', {
            url: "/app/news/:id/",
            data : { pageTitle: 'News' },
            templateUrl: "app/components/news/newspage.html",
            controller: 'NewsPageController',
            controllerAs: 'vm'
        });
    }

})();
