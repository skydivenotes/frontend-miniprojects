(function () {
    'use strict';

    angular
    .module('tNotes')
    .controller('NewsController', NewsController);

    function NewsController($rootScope, $filter, NewsService, sharedProperties, LogService) {
        // set page title
        $rootScope.pageTitle = $filter('translate')("NEWS");

        var vm = this, _news = [], offset = 10, currentOffset = offset;

        NewsService.getNews().then(function (news) {
            _news = news;
            vm.paging();
        });

        vm.paging = function () {
            currentOffset += offset;
            vm.news = _news.slice(0, currentOffset);
        }

        vm.debug_url = "";
        vm.filters = sharedProperties.getValue("filters.news");
        vm.debug_url = LogService.debugParams("news", vm.filters);
        $rootScope.$on("filters.news", function ($event, filters) {
            vm.debug_url = LogService.debugParams("news", filters);
        });

    }

})();
