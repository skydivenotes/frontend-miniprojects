(function () {
    'use strict';

    angular
    .module('tNotes')
    .service('NewsService', NewsService);

    function NewsService($http, API_URL) {
        var service = {};

        angular.extend(service, {
            getNews: getNews,
            getNewsById: getNewsById
        });

        function getNews() {
            return $http.get(API_URL.GET_NEWS).then(function (response) {
                response.data.map(function(item){
                    item.date = new Date(item.date);
                });
                return response.data;
            });
        }

        function getNewsById(id) {
            return $http.get(API_URL.GET_NEWS).then(function (response) {
                return response.data.filter(function(item){
                    return item.id == id;
                })[0];
            });
        }

        return service;
    }

})();
