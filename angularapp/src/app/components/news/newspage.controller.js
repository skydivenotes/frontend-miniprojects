(function () {
    'use strict';

    angular
    .module('tNotes')
    .controller('NewsPageController', NewsPageController);

    function NewsPageController(NewsService, $stateParams) {
        var vm = this;


        NewsService.getNewsById($stateParams.id).then(function(data){
            vm.news = data;
        });
    }

})();
