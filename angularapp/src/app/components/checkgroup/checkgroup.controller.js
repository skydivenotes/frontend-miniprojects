(function () {
    'use strict';

    angular
    .module('tNotes')
    .controller('CheckgroupController', CheckgroupController);

    function CheckgroupController($scope) {
        var vm = this;
        vm.items = $scope.items;
        vm.placeholder = $scope.placeholder;
        $scope.$watch(function(){return vm.ngModel}, function(val){
           $scope.ngModel = val;
        }, true);
    }

})();
