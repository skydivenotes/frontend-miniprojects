(function () {
    'use strict';

    angular
    .module('tNotes')
    .directive('checkGroup', checkGroup);

    function checkGroup() {
        var directive = {
            restrict: "E",
            transclude: true,
            scope: {
                ngModel: '=',
                placeholder: '@',
                items: '='
            },
            templateUrl: "app/components/checkgroup/checkgroup.html",
            controller: 'CheckgroupController',
            controllerAs: 'vm'
        };

        return directive;
    }

})();
