(function () {
    'use strict';

    angular
    .module('tNotes')
    .controller('SaleController', SaleController);

    function SaleController($rootScope, SaleService, $scope, sharedProperties, LogService) {

        $rootScope.pageTitle = "Sales";

        var vm = this;
        vm.selectedIndex = 0;
        vm.dygraph = null;

        vm.charts = [
            {
                id: "total",
                title: "Total",
                tab: 0
            },
            {
                id: "onsite",
                title: "Web vs OnSite",
                tab: 2
            },
            {
                id: "product",
                title: "Products",
                tab: 3
            },
            {
                id: "component",
                title: "Components",
                tab: 1
            }
        ];

        SaleService.getLogChart().then(function (options) {
            vm.charts.map(function (chart) {
                var options1 = angular.copy(options);
                var options2 = angular.copy(options);
                options2.panels[0].stockGraphs.map(function (graph) {
                    graph.stackable = false;
                });
                AmCharts.makeChart(chart.id + "-stack", angular.copy(options1));
                AmCharts.makeChart(chart.id + "-bar", angular.copy(options2));
            });
        });

        SaleService.getLogPieChart().then(function (options) {
            vm.charts.map(function (chart) {
                AmCharts.makeChart(chart.id + "-pie", options);
            });
        });

        SaleService.getDygraphChart().then(function (options) {
            vm.charts.map(function (chart) {
                var dygraph = new Dygraph(document.getElementById(chart.id + "-line"), angular.copy(options), {
                    showRoller: true,
                    customBars: true,
                    labelsDivStyles: {'textAlign': 'right'},
                    showRangeSelector: true,
                    rangeSelectorHeight: 30,
                    rangeSelectorPlotStrokeColor: 'darkred',
                    rangeSelectorPlotFillColor: '#CC4748'
                });
                // watch a selected index
                $scope.$watch(function () {
                    return chart.tab
                }, function (value) {
                    // redraw dychart
                    if (value === 3 && dygraph) {
                        setTimeout(function () {
                            dygraph.resize();
                        });
                    }
                });
            });
        });


        vm.debug_url = "";
        vm.filters = sharedProperties.getValue("filters.sale");
        vm.debug_url = LogService.debugParams("sale", vm.filters);
        $rootScope.$on("filters.sale", function ($event, filters) {
            vm.debug_url = LogService.debugParams("sale", filters);
        });


    }


})();
