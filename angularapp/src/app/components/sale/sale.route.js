(function () {
    'use strict';

    angular
    .module('tNotes')
    .config(routerConfig);

    function routerConfig($stateProvider) {
        $stateProvider
        .state('sale', {
            url: "/app/sale",
            templateUrl: "app/components/sale/sale.html",
            controller: 'SaleController',
            controllerAs: 'vm'
        });
    }

})();
