(function () {
    'use strict';

    angular
    .module('tNotes')
    .service('SaleService', SaleService);

    function SaleService($http, $q, logChartOptions, API_URL) {
        var service = {};
        angular.extend(service, {
            getLogPieChart: getLogPieChart,
            getLogChart: getLogChart,
            getDygraphChart: getDygraphChart
        });

        function getLogChart() {
            return $http.get("assets/sales.chart.json").then(function (response) {
                return response.data;
            });
        }

        function getLogPieChart() {
            return $http.get("assets/sales.piechart.json").then(function (response) {
                return response.data;
            });
        }

        function getDygraphChart() {
            return $http.get("assets/dygraph.json").then(function (response) {
                var data = response.data;
                data.map(function(item){
                    item[0] = new Date(item[0]);
                });
                return response.data;
            });
        }

        return service;
    }

})();
