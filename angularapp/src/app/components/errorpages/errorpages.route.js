(function () {
    'use strict';

    angular
        .module('tNotes')
        .config(routerConfig);

    function routerConfig($stateProvider) {
        $stateProvider
            .state('401', {
                url: '/401',
                templateUrl: 'app/components/errorpages/401.html',
                controller: 'LoginController',
                controllerAs: 'vm',
                data: {}
            });
    }

})();
