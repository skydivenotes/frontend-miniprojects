(function () {
    'use strict';

    angular
    .module('tNotes')
    .controller('ChipsautoController', ChipsautoController);

    function ChipsautoController($http, $scope, $interpolate, API_URL) {
        var vm = this;
        vm.query = "";
        vm.items = [];

        var params = {
            requestUrl: API_URL.GET_CHIPS_USERS,
            templateUrl: 'app/components/chipsauto/chipsauto.html',
            model: 'id'
        };

        switch ($scope.chipsType) {
            case 'product_code':
                params.requestUrl = API_URL.GET_CHIPS_PRODUCTCODE;
                params.templateUrl = 'app/components/chipsauto/chipsauto_productcode.html';
                params.model = 'code';
                break;
        }

        $scope.getContentUrl = function () {
            return params.templateUrl;
        };

        $scope.$watch(function () {
            return vm.items
        }, function (val) {
            $scope.ngModel = val.map(function (item) {
                return item[params.model];
            });
        }, true);

        vm.getMatches = function (val) {
            var url = $interpolate(params.requestUrl)({
                query: val
            });
            vm.ngModel = val;
            return $http({
                method: 'GET',
                url: url,
                params: {
                    "type": $scope.chipsType
                }
            }).then(function (response) {
                return response.data.slice(0, 10);
            });
        };
    }

})();
