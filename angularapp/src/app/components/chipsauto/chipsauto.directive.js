(function () {
    'use strict';

    angular
    .module('tNotes')
    .directive('chipsauto', chipsauto);

    function chipsauto() {
        var directive = {
            restrict: "E",
            transclude: true,
            scope: {
                ngModel: '=',
                placeholder: '@',
                chipsType: '@'
            },

            template: '<div ng-include="getContentUrl()"></div>',
            controller: 'ChipsautoController',
            controllerAs: 'vm'
        };

        return directive;
    }

})();
