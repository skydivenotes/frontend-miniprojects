(function () {
    'use strict';

    angular
    .module('tNotes')
    .controller('CustomHeaderController', CustomHeaderController);

    function CustomHeaderController($scope) {
        var vm = this;

        angular.extend(vm, {
            showSearchOverlay: showSearchOverlay
        });

        // Broadcasts a message to pgSearch directive to toggle search overlay
        function showSearchOverlay() {
            $scope.$broadcast('toggleSearchOverlay', {
                show: true
            })
        }

    }

})
();
