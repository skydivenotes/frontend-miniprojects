(function () {
    'use strict';

    angular
    .module('tNotes')
    .directive('customHeader', customHeader);

    function customHeader() {
        var directive = {
            restrict: "E",
            transclude: true,
            templateUrl: "app/components/header/header.html",
            controller: 'CustomHeaderController',
            controllerAs: 'header'
        }

        return directive;
    }

})();
