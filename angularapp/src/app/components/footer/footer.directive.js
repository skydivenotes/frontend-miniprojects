(function () {
    'use strict';

    angular
    .module('tNotes')
    .directive('customFooter', customFooter);

    function customFooter() {
        var directive = {
            restrict: "E",
            transclude: true,
            templateUrl: "app/components/footer/footer.html",
            controller: 'FooterController',
            controllerAs: 'footer'
        }

        return directive;
    }

})();
