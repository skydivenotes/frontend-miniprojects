(function () {
    'use strict';

    angular
    .module('tNotes')
    .controller('QuickSearchController', QuickSearchController);

    function QuickSearchController($log) {
        var vm = this;

        angular.extend(vm, {
            liveSearch: liveSearch
        });

        function liveSearch() {
            $log.info("Live search for: " + vm.search.query);
        }

    }

})();
