(function () {
    'use strict';

    angular
    .module('tNotes')
    .directive('quickSearch', quickSearch);

    function quickSearch() {
        var directive = {
            restrict: "E",
            transclude: true,
            templateUrl: "app/components/quicksearch/quicksearch.html",
            controller: 'QuickSearchController',
            controllerAs: 'quicksearch'
        }

        return directive;
    }

})();
