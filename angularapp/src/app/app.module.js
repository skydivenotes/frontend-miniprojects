(function () {
    'use strict';

    angular
    .module('tNotes', [
        'ngAnimate',
        'ngCookies',
        'ngSanitize',
        'ngMaterial',
        'md.data.table',
        'ngMessages',
        'ngResource',
        'ui.router',
        'ui.bootstrap',
        'ui.utils',
        'ui.select',
        'pascalprecht.translate',
        'daterangepicker',
        'formly',
        'formlyBootstrap',
        'amChartsDirective',
        'google.places',
        'infinite-scroll',
        'froala',
        'angular-websocket'
    ]);

})();
