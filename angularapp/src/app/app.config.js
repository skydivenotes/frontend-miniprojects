(function () {
    'use strict';

    angular
    .module('tNotes')
    .config(config);

    function config($logProvider, $translateProvider, $httpProvider) {
        $httpProvider.defaults.withCredentials = true;
        $httpProvider.defaults.headers.common = {};
        $httpProvider.defaults.headers.post = {};
        $httpProvider.defaults.headers.put = {};
        $httpProvider.defaults.headers.patch = {};
        // Enable log
        $logProvider.debugEnabled(true);

        // Localization
        $translateProvider
        .useStaticFilesLoader({
            prefix: 'assets/locales/locale_',
            suffix: '.json'
        })
        .registerAvailableLanguageKeys(['en', 'fr'], {
            'en': 'en',
            'fr': 'fr'
        })
        .preferredLanguage('en')
        .fallbackLanguage('en')
        .useMissingTranslationHandlerLog()
        .usePostCompiling(true)
        .useSanitizeValueStrategy('sanitize')
        .useLocalStorage();

        //Catch 401 Error from server
        $httpProvider.interceptors.push('HttpService');
        $httpProvider.useApplyAsync(true);
    }

})();
