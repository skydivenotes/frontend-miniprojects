(function () {
    'use strict';

    angular
        .module('tNotes')
        .service('MainService', MainService);

    function MainService($q, $http, $interpolate, $websocket, API_URL) {
        var service = {};

        angular.extend(service, {
            getUserDetails: getUserDetails,
            openWebSocket: openWebSocket
        });

        return service;

        function getUserDetails() {
            var url = API_URL.GET_CURRENT_USER;
            var deferred = $q.defer();

            $http.get(url, {}).then(function (response) {
                deferred.resolve(angular.fromJson(response.data));
            }).catch(function (error) {
                deferred.reject(angular.fromJson(error.data));
            });
            return deferred.promise;
        }

        function openWebSocket() {
            // Open a WebSocket connection
            var dataStream = $websocket(API_URL.GET_WEBSOCKET);

            var collection = [];

            dataStream.onMessage(function (message) {
                collection.push(JSON.parse(message.data));
            });

            var methods = {
                collection: collection,
                get: function () {
                    dataStream.send(JSON.stringify({action: 'get'}));
                }
            };

            return methods;
        }

    }

})();
