(function () {
    'use strict';

    angular
        .module('tNotes')
        .controller('MainController', MainController);

    function MainController($rootScope, $state, MainService) {
        var vm = this;

        // set page title
        $rootScope.pageTitle = "TunnelNotes";

        angular.extend(vm, {
            message: "How are you doing today?",
            webSocketData: MainService.openWebSocket(),
            is: is,
            includes: includes
        });

        // Checks if the given state is the current state
        function is(name) {
            return $state.is(name);
        }

        // Checks if the given state/child states are present
        function includes(name) {
            return $state.includes(name);
        }
    }

})();
