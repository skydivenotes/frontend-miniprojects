(function () {
        'use strict';

        angular
        .module('tNotes')
        .directive('customMouseUp', customMouseUp)
        .directive('csSelect', csSelect)
        .directive('pgDropdown', pgDropdown)
        .directive('pgFormGroup', pgFormGroup)
        .directive('pgNavigate', pgNavigate)
        .directive('pgPortlet', pgPortlet)
        .directive('pgSearch', pgSearch)
        .directive('pgSidebar', pgSidebar)
        .directive('pgTab', pgTab)
        .directive('skycons', skycons)
        .directive('includeReplace', includeReplace)
        .directive('stopEvent', stopEvent)
        .directive('visible', visible)
        .directive('mdCheckboxWrapper', mdCheckboxWrapper);

        function stopEvent() {
            return {
                restrict: 'A',
                link: function (scope, element, attr) {
                    if (attr && attr.stopEvent)
                        element.bind(attr.stopEvent, function (e) {
                            e.stopPropagation();
                        });
                }
            };
        }

        function visible() {
            return {
                restrict: 'A',
                link: function (scope, element, attributes) {
                    scope.$watch(attributes.visible, function (value) {
                        element.css('visibility', value ? 'visible' : 'hidden');
                    });
                }
            }
        }

        function mdCheckboxWrapper() {
            return {
                restrict: 'A',
                link: function (scope, el, attrs) {
                    $(el).find(".md-icon").css("background-color", attrs.color);
                }
            };
        }


        /* ============================================================
         * Directive: csSelect
         * AngularJS directive for SelectFx jQuery plugin
         * https://github.com/codrops/SelectInspiration
         * ============================================================ */

        function customMouseUp() {

            return {
                scope: {
                    customMouseUp: "=customMouseUp"
                },
                link: function (scope, element, attrs) {

                    element.bind("mouseup", function () {
                        var selection;
                        if (window.getSelection) {
                            selection = window.getSelection();
                        } else if (document.selection) {
                            selection = document.selection.createRange();
                        }
                        if (!selection.toString())
                            scope.customMouseUp = !scope.customMouseUp;
                        scope.$apply();
                    })

                }
            }
        }

        function csSelect() {
            return {
                restrict: 'A',
                link: function (scope, el, attrs) {
                    if (!window.SelectFx) return;

                    var el = $(el).get(0);
                    $(el).wrap('<div class="cs-wrapper"></div>');
                    new SelectFx(el);

                }
            }
        }

        /* ============================================================
         * Directive: pgDropdown
         * Prepare Bootstrap dropdowns to match Pages theme
         * ============================================================ */

        function pgDropdown() {
            return {
                restrict: 'A',
                link: function (scope, element, attrs) {

                    var btn = $(element).find('.dropdown-menu').siblings('.dropdown-toggle');
                    var offset = 0;

                    var padding = btn.actual('innerWidth') - btn.actual('width');
                    var menuWidth = $(element).find('.dropdown-menu').actual('outerWidth');

                    if (btn.actual('outerWidth') < menuWidth) {
                        btn.width(menuWidth - offset);
                        $(element).find('.dropdown-menu').width(btn.actual('outerWidth'));
                    } else {
                        $(element).find('.dropdown-menu').width(btn.actual('outerWidth'));
                    }

                }
            }
        }

        /* ============================================================
         * Directive: pgFormGroup
         * Apply Pages default form effects
         * ============================================================ */

        function pgFormGroup() {
            return {
                restrict: 'A',
                link: function (scope, element, attrs) {
                    $(element).on('click', function () {
                        $(this).find(':input').focus();
                    });
                    $(element).find(':input').on('focus', function () {
                        $('.form-group.form-group-default').removeClass('focused');
                        $(element).addClass('focused');
                    });
                    $(element).find(':input').on('blur', function () {
                        $(element).removeClass('focused');
                        if ($(this).val()) {
                            $(element).find('label').addClass('fade');
                        } else {
                            $(element).find('label').removeClass('fade');
                        }
                    });
                    $(element).find('.checkbox, .radio').hover(function () {
                        $(this).parents('.form-group').addClass('focused');
                    }, function () {
                        $(this).parents('.form-group').removeClass('focused');
                    });
                }
            }
        }

        /* ============================================================
         * Directive: pgNavigate
         * Pre-made view ports to be used for HTML5 mobile hybrid apps
         * ============================================================ */

        function pgNavigate() {
            return {
                restrict: 'A',
                link: function (scope, element, attrs) {

                    $(element).click(function () {
                        var el = $(this).attr('data-view-port');
                        if ($(this).attr('data-toggle-view') != null) {
                            $(el).children().last().children('.view').hide();
                            $($(this).attr('data-toggle-view')).show();
                        }
                        $(el).toggleClass($(this).attr('data-view-animation'));
                        return false;
                    });


                }
            }
        }

        /* ============================================================
         * Directive: pgPortlet
         * AngularJS directive for Pages Portlets jQuery plugin
         * ============================================================ */

        function pgPortlet($parse) {
            return {
                restrict: 'A',
                scope: true,
                link: function (scope, element, attrs) {

                    var onRefresh = $parse(attrs.onRefresh);

                    var options = {};

                    if (attrs.progress) options.progress = attrs.progress;
                    if (attrs.overlayOpacity) options.overlayOpacity = attrs.overlayOpacity;
                    if (attrs.overlayColor) options.overlayColor = attrs.overlayColor;
                    if (attrs.progressColor) options.progressColor = attrs.progressColor;
                    if (attrs.onRefresh) options.onRefresh = function () {
                        onRefresh(scope);
                    };

                    element.portlet(options);

                    scope.maximize = function () {
                        element.portlet('maximize');
                    }
                    scope.refresh = function () {
                        element.portlet({
                            refresh: true
                        });
                    }
                    scope.close = function () {
                        element.portlet('close');
                    }
                    scope.collapse = function () {
                        element.portlet('collapse');
                    }
                }
            }
        }

        /* ============================================================
         * Directive: pgSearch
         * AngularJS directive for Pages Overlay Search jQuery plugin
         * ============================================================ */

        function pgSearch($parse) {
            return {
                restrict: 'A',
                link: function (scope, element, attrs) {
                    $(element).search();

                    scope.$on('toggleSearchOverlay', function (scopeDetails, status) {
                        if (status.show) {
                            $(element).data('pg.search').toggleOverlay('show');
                        } else {
                            $(element).data('pg.search').toggleOverlay('hide');
                        }
                    })

                }
            }
        }

        /* ============================================================
         * Directive: pgSidebar
         * AngularJS directive for Pages Sidebar jQuery plugin
         * ============================================================ */

        function pgSidebar() {
            return {
                restrict: 'A',
                link: function (scope, element, attrs) {
                    var $sidebar = $(element);
                    $sidebar.sidebar($sidebar.data());

                    // Bind events
                    // Toggle sub menus
                    $('body').on('click', '.sidebar-menu a', function (e) {

                        if ($(this).parent().children('.sub-menu') === false) {
                            return;
                        }
                        var el = $(this);
                        var parent = $(this).parent().parent();
                        var li = $(this).parent();
                        var sub = $(this).parent().children('.sub-menu');

                        if (li.hasClass("active open")) {
                            el.children('.arrow').removeClass("active open");
                            sub.slideUp(200, function () {
                                li.removeClass("active open");
                            });

                        } else {
                            parent.children('li.open').children('.sub-menu').slideUp(200);
                            parent.children('li.open').children('a').children('.arrow').removeClass('active open');
                            parent.children('li.open').removeClass("open active");
                            el.children('.arrow').addClass("active open");
                            sub.slideDown(200, function () {
                                li.addClass("active open");

                            });
                        }
                    });

                }
            }
        }

        /* ============================================================
         * Directive: pgTab
         * Makes Bootstrap Tabs compatible with AngularJS and add sliding
         * effect for tab transitions.
         * ============================================================ */

        function pgTab($parse) {
            return {
                link: function (scope, element, attrs) {
                    var slide = attrs.slide;
                    var onShown = $parse(attrs.onShown);
                    // Sliding effect for tabs
                    $(element).on('show.bs.tab', function (e) {
                        e = $(e.target).parent().find('a[data-toggle=tab]');

                        var hrefCurrent = e.attr('href');

                        if ($(hrefCurrent).is('.slide-left, .slide-right')) {
                            $(hrefCurrent).addClass('sliding');

                            setTimeout(function () {
                                $(hrefCurrent).removeClass('sliding');
                            }, 100);
                        }
                    });

                    $(element).on('shown.bs.tab', {
                        onShown: onShown
                    }, function (e) {
                        if (e.data.onShown) {
                            e.data.onShown(scope);
                        }
                    });

                    element.click(function (e) {
                        e.preventDefault();
                        $(element).tab('show');
                    });
                }
            }
        }

        /* ============================================================
         * Directive: skycons
         * AngularJS directive for skycons plugin
         * http://darkskyapp.github.io/skycons/
         * ============================================================ */

        function skycons() {
            return {
                restrict: 'A',
                link: function (scope, element, attrs) {
                    var skycons = new Skycons();
                    skycons.add($(element).get(0), attrs.class);
                    skycons.play();
                }
            }
        }

        /*
         Use this directive together with ng-include to include a
         template file by replacing the placeholder element
         */

        function includeReplace() {
            return {
                require: 'ngInclude',
                restrict: 'A',
                link: function (scope, el, attrs) {
                    el.replaceWith(el.children());
                }
            }
        }

    })();
