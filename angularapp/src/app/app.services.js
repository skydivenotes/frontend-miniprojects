(function () {
    'use strict';

    angular
        .module('tNotes')
        .service("sharedProperties", sharedProperties)
        .service('HttpService', HttpService);

    function sharedProperties() {
        var hashtable = {};
        return {
            /**
             * Set value for shared property by its name.
             * @paran {string} property name
             * @param {Object} property vale.
             */
            setValue: function (key, value) {
                hashtable[key] = value;
            },
            getValue: function (key) {
                return hashtable[key];
            }
        }
    }

    function HttpService($rootScope, $q) {
        var service = {};

        service.responseError = responseError;

        return service;

        function responseError(response) {
            if (response.status === 401) {
                $rootScope.$broadcast('unauthorized');
            }
            return $q.reject(response);
        }
    }

})();
