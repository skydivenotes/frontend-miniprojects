(function () {
    'use strict';

    angular
    .module('tNotes')
    .run(runBlock);

    function runBlock($rootScope, $window, API_URL, formlyConfig, MainService, $filter) {
        var scope = $rootScope;

        $rootScope.$on("menuItemSelect", function ($event, data) {
            $rootScope.pageTitle = $filter('translate')(data.translateKey);
        });


        formlyConfig.setType({
            name: 'datepicker',
            template: '<md-datepicker ng-model="model[options.key]" md-placeholder="{{to.label}}"></md-datepicker>'
        });

        formlyConfig.setType({
            name: 'select',
            overwriteOk: true,
            template: '<md-input-container><label>{{to.label}}</label><md-select ng-model="model[options.key]"><md-option ng-repeat="option in to.options" value="{{option.value}}">{{option.title}}</md-option></md-select>'
        });


        formlyConfig.setType({
            name: 'select-api-multiple',
            template: '<md-input-container><label></label><chipsauto ng-model="model[options.key]" placeholder="{{to.label}}"></chipsauto></md-input-container>'
        });

        formlyConfig.setType({
            name: 'address',
            template: '<md-input-container><label>{{to.label}}</label><input type="text" g-places-autocomplete ng-model="model[options.key]"/></md-input-container>',
            overwriteOk: true
        });

        formlyConfig.setType({
            name: 'input',
            template: '',
            overwriteOk: true
        });

        formlyConfig.setType({
            name: 'checkbox',
            template: '<md-checkbox ng-model="model[options.key]">{{to.label}}</md-checkbox>',
            overwriteOk: true
        });

        formlyConfig.setType({
            name: 'textarea',
            template: "<md-input-container><label>{{to.label}}</label><textarea ng-model=\"model[options.key]\" rows=\"4\"></textarea></md-input-container>",
            overwriteOk: true
        });

        formlyConfig.setWrapper({
            name: 'mdLabel',
            types: ['input'],
            template: '<label>{{to.label}}</label><formly-transclude></formly-transclude>',
            overwriteOk: true
        });

        formlyConfig.setWrapper({
            name: 'mdInputContainer',
            types: ['input'],
            template: '<md-input-container><formly-transclude></formly-transclude></md-input-container>',
            overwriteOk: true
        });

        // having trouble getting icons to work.
        // Feel free to clone this jsbin, fix it, and make a PR to the website repo: https://github.com/formly-js/angular-formly-website
        formlyConfig.templateManipulators.preWrapper.push(function (template, options) {
            if (!options.data.icon) {
                return template;
            }
            return '<md-icon class="step" md-font-icon="icon-' + options.data.icon + '"></md-icon>' + template;
        });

        scope.API_URL = API_URL;

        MainService.getUserDetails().then(function (data) {
            //console.log(data); // data is success response from back-end you can make any manipulations
        }).catch(function (error) {
            //console.log(error); // error is error response from back-end you make some logs or show error message for customer
        });

        scope.$on('unauthorized', function () {
            /*$state.go("401", {}, {
             location: false
             });*/
            $window.location.href = 'http://my.dev.skydivenotes.com/sign-in#';
        });

    }

})();
